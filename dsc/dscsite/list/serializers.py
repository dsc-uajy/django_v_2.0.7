from . import models
from rest_framework import serializers
class dataBarangSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.dataBarang
        fields = (
            'nama',
            'harga',
            'status'
        )