from django.db import models

# Create your models here.
class dataBarang(models.Model):
    nama = models.CharField(max_length=256)
    harga = models.IntegerField(default=0)
    status = models.CharField(max_length=20)