from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^barang/list/$', views.dataBarangList.as_view(), name="list"),
    url(r'^barang/create/$', views.dataBarangCreate.as_view(), name="create"),
    url(r'^barang/detail/(?P<pk>\d+)/$', views.dataBarangDetail.as_view(), name="detail"),
    url(r'^barang/$', views.dataBarangListCreate.as_view(), name="listcreate")
]

urlpatterns = format_suffix_patterns(urlpatterns)