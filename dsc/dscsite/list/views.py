from django.shortcuts import render

from . import models, serializers, paginations
# Create your views here.

from rest_framework import generics, permissions
from rest_framework.views import APIView

class dataBarangList(generics.ListAPIView):
    pagination_class = paginations.dataPageNumberPagination
    queryset = models.dataBarang.objects.all()
    serializer_class = serializers.dataBarangSerializer

class dataBarangCreate(generics.CreateAPIView):
    pagination_class = paginations.dataPageNumberPagination
    queryset = models.dataBarang.objects.all()
    serializer_class = serializers.dataBarangSerializer

class dataBarangDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.dataBarang.objects.all()
    serializer_class = serializers.dataBarangSerializer

class dataBarangListCreate(generics.ListCreateAPIView):
    pagination_class = paginations.dataPageNumberPagination
    queryset = models.dataBarang.objects.all()
    serializer_class = serializers.dataBarangSerializer